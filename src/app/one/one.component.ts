import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../service/service.service';
import { HttpClient } from '@angular/common/http';
import {Chart} from 'chart.js'
import { from } from 'rxjs';
export interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.css']
})

export class OneComponent implements OnInit {
  list:any;
  chart:any;
  lineChart:any;
  HelloChart:any;
  Mostly:any;
  partly:any;
  shower:any;
  public results:string[]
  constructor(private api:ServiceService) {
  }

  ngOnInit() {
    this.api.get_data().subscribe(res=>{
      console.log(res)
      this.list = res

      this.chart = new Chart('myChart',{
        type: 'horizontalBar',
        data: {
            labels: ['fine','partlyCloud','mostlyCloud','shower','cloudy'],
            datasets: [{
                label: 'Season',
                backgroundColor: [
                  '#003f5c',
                  '#58508d',
                  '#bc5090',
                  '#ff6361',
                  '#ffa600'

                ],
                borderColor: 'rgb(255, 99, 132)',
                data: [res['fine'].toFixed(0),
                       res['partlycloudy'].toFixed(0),
                       res['mostlycloudy'].toFixed(0),
                       res['shower'].toFixed(0),
                       res['cloudy'].toFixed(0)]
            }]
        },
        options: {}
      })
    })
    this.api.getdata_test().subscribe(res=>{
      this.api.getdata_predict().subscribe(resp =>{
        console.log(resp['pre_cloudy'].length)
        console.log(res['test_cloudy'].length)
        let count = []
        for(let i = 0 ; i < resp['pre_cloudy'].length;i++){
          count.push(i)
        }
        this.lineChart = new Chart('lineChart',{
          type: 'line',
          data: {
              labels: count,
              datasets: [{
                  label: 'datatest',
                  backgroundColor: [
                    '#003f5c'
                  ],
                  borderColor: 'rgb(255, 99, 132)',
                  data: res['test_cloudy'],
              },
              {
                label: 'predictdata',
                  backgroundColor: [
                    '#fffff'
                  ],
                  borderColor: 'rgb(255, 99, 132)',
                  data: resp['pre_cloudy']
              }
            ]
          },
          options: {}
        })
        let count_ = []
        for(let i = 0 ; i < res['test_fine'].length;i++){
          count_.push(i)
        }
        this.HelloChart = new Chart('HelloChart',{
          type: 'line',
          data: {
              labels: count_,
              datasets: [{
                  label: 'datatest',
                  backgroundColor: [
                    '#003f5c'
                  ],
                  borderColor: 'rgb(255, 99, 132)',
                  data: res['test_fine'],
              },
              {
                label: 'predictdata',
                  backgroundColor: [
                    '#fffff'
                  ],
                  borderColor: 'rgb(255, 99, 132)',
                  data: resp['pre_fine']
              }
            ]
          },
          options: {}
        })
        let count1 = []
        for(let i = 0 ; i < res['test_mostlycloudy'].length;i++){
          count1.push(i)
        }
        this.Mostly = new Chart('Mostly',{
          type: 'line',
          data: {
              labels: count1,
              datasets: [{
                  label: 'datatest',
                  backgroundColor: [
                    '#003f5c'
                  ],
                  borderColor: 'rgb(255, 99, 132)',
                  data: res['test_mostlycloudy'],
              },
              {
                label: 'predictdata',
                  backgroundColor: [
                    '#fffff'
                  ],
                  borderColor: 'rgb(255, 99, 132)',
                  data: resp['pre_mostlycloudy']
              }
            ]
          },
          options: {}
        })
        let count2 = []
        for(let i = 0 ; i < res['test_partlycloudy'].length;i++){
          count2.push(i)
        }
        this.partly = new Chart('partly',{
          type: 'line',
          data: {
              labels: count2,
              datasets: [{
                  label: 'datatest',
                  backgroundColor: [
                    '#003f5c'
                  ],
                  borderColor: 'rgb(255, 99, 132)',
                  data: res['test_partlycloudy'],
              },
              {
                label: 'predictdata',
                  backgroundColor: [
                    '#fffff'
                  ],
                  borderColor: 'rgb(255, 99, 132)',
                  data: resp['pre_partlycloudy']
              }
            ]
          },
          options: {}
        })

        let count3 = []
        for(let i = 0 ; i < res['test_shower'].length;i++){
          count3.push(i)
        }
        this.shower = new Chart('shower',{
          type: 'line',
          data: {
              labels: count3,
              datasets: [{
                  label: 'datatest',
                  backgroundColor: [
                    '#003f5c'
                  ],
                  borderColor: 'rgb(255, 99, 132)',
                  data: res['test_shower'],
              },
              {
                label: 'predictdata',
                  backgroundColor: [
                    '#fffff'
                  ],
                  borderColor: 'rgb(255, 99, 132)',
                  data: resp['pre_shower']
              }
            ]
          },
          options: {}
        })
      })
    })
  }
  
  foods: Food[] = [
    {value: 'fine', viewValue: 'Fine'},
    {value: 'partyCloud', viewValue: 'Partly Cloud'},
    {value: 'mostlyCloud', viewValue: 'Mostly Cloud'},
    {value: 'shower', viewValue: 'Shower'},
    {value: 'cloudy', viewValue: 'Cloudy'}
  ];

  onChange(value){
    let body = document.getElementById('head')
    var show = document.getElementById('show')
    console.log(body.classList)
    // if(value.value == "partyCloud"){
    // body.className = "sunny cardregister example-container"
    switch(value.value){
      case "fine":
        body.className = "sunny cardregister example-container"
        show.innerHTML = this.list['fine'].toFixed(2);    
        break;
      case "partyCloud":
        body.className = "partly cardregister example-container"
        show.innerHTML = this.list['partlycloudy'].toFixed(2);  
        break;
      case "mostlyCloud":
        body.className = "mostly cardregister example-container"
        show.innerHTML = this.list['mostlycloudy'].toFixed(2);  
        break;
      case "shower":
        body.className = "shower cardregister example-container"
        show.innerHTML = this.list['shower'].toFixed(2); 
        break;
      case "cloudy":
        body.className = "cloudy cardregister example-container"
        show.innerHTML = this.list['cloudy'].toFixed(2); 
        break;
      default:
        body.className = "bg cardregister example-container"
        show.innerHTML = "Solar"
        console.log("home")
        break;
    }
  }

}
