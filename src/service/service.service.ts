import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  constructor(private http: HttpClient) { }
  get_data(){
    return this.http.get("http://127.0.0.1:4080/generator")
    .pipe(map(result => result));
  }

  getdata_test(){
    return this.http.get("http://127.0.0.1:4080/predict")
    .pipe(map(result => result));
  }
  getdata_predict(){
    return this.http.get("http://127.0.0.1:4080/afterpredict")
    .pipe(map(result => result));
  }

}
